FROM openjdk:7-jre-alpine3.9

WORKDIR /target
COPY target/run.sh .
COPY ./ /target/


EXPOSE 8080
CMD ["run.sh"]

